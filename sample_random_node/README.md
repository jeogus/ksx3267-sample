# Sample_random_node 

온습도를 랜덤으로 생성하는 KS-X-3267 표준 샘플 노드


# 소개 
본 프로젝트는 KS-X-3267 표준으로 통신이 가능한 샘플노드를 제작하는 것이다. random 함수를 이용하여 온습도 값을 랜덤으로 발생시키고, 표준으로 전달할 수 있는 샘플노드를 개발/공개 하였다.


# 샘플센서노드의 모드버스 맵
* 장비정보 
  * 2 : 회사코드  (주식회사 지농 : 1)
  * 3 : 제품타입  (노드  : 1)
  * 4 : 제품코드  (샘플노드 : 10001)
  * 5 : 프로토콜버전  (KS-X-3267 : 101)
  * 6 : 연결장비수  (2) : 온도 , 습도 
  * 7 : 구역수  (-)

* 연결제품코드 
  * 101  : 장치코드 1  (온도)
  * 102  : 장치코드 2  (습도)

* 노드상태 
  * 201  : opid  (-)
  * 202  : 노드상태  (-)

* 센서정보 
  * 211~212 : 온도센서값  
  * 213 : 온도센서상태 
  * 221~222  : 습도센서값  
  * 223 : 습도센서상태 

# 개발  관련  공통  사항 

* 라이브러리 
  * [ModbusSlave](https://github.com/yaacov/ArduinoModbusSlave) : Modbus 라이브러리 

* 개발환경 
  * 아두이노 

* 테스트 장비
  * Arduino Uno

# 코드 예시
아래의 코드는 상황에 따라 바꿔야하는 코드입니다.

```
    uint16_t  devinfo [ 13 ] = {101 , 202 };
    pushValue (40101 , 13 , address , devinfo );  
```
 13이 들어간 자리에 연결된 장치 수를 입력. (최대 13개의 장치 연결 가능) 
 
 연결된 장비 수가 5개 라면 아래와 같이 코드를 수정합니다.
 ```
    uint16_t  devinfo [ 5 ] = {101 , 202 };
    pushValue (40101 , 5 , address , devinfo );  
```
 

# 장비 스펙
샘플센서노드의 장비스펙은 다음과 같다.
```
{
  "Class" : "node",
  "Type" : "sensor-node",
  "Model" : "SENSOR_NODE_001",
  "Name" : "센서 노드",
  "CommSpec" : {
    "KS-X-3267:2018" : {
      "read": {"starting-register":201, "items":["opid","status"]},
      "write": {"starting-register":001, "items":["operation","opid"]}
    }
  },
  "Devices": [{
    "Class" : "sensor",
    "Type" : "temperature sensor",
    "Model" : "DHT22_TEMP",
    "Name" : "온도 센서",
    "ValueUnit" : 1,
    "ValueType" : "float",
    "CommSpec" : {
      "KS-X-3267:2018" : {
        "read": {"starting-register":211, "items":["value", "status"]}
      }
    }
  }, {
    "Class" : "sensor",
    "Type" : "humidity sensor",
    "Model" : "DHT22_HUMI",
    "Name" : "습도 센서",
    "ValueUnit" : 2,
    "ValueType" : "float",
    "CommSpec" : {
      "KS-X-3267:2018" : {
        "read": {"starting-register":221, "items":["value", "status"]}
      }
    }
  }]
}
```

```
