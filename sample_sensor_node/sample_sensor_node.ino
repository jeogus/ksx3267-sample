/*

@file sample_sensor_node.ino
@data 2019-06-28

Copyright (c) 2019 JiNong, Inc.
All right reserved.

*/

#include <ModbusSlave.h>
#include <SoftwareSerial.h>
#include "DHT.h"

/*
 DHT22 사용법
 1번핀 +5V, 2번핀 DHTPIN, 4번핀 GROUND
*/
#define DHTPIN 5    //dht22(온습도센서) 연결 핀 번호
#define DHTTYPE DHT22 

/* 
 slave id = 1, control-pin = 8, baud = 9600 
*/
#define SLAVE_ID 	1
#define CTRL_PIN 	8
#define BAUDRATE 	9600

DHT dht(DHTPIN, DHTTYPE);
Modbus slave(SLAVE_ID, CTRL_PIN);

void setup() {
    slave.cbVector[CB_READ_HOLDING_REGISTERS] = readMemory;

    Serial.begin(9600);
    slave.begin(9600);
    dht.begin();

    pinMode(5,INPUT); 
}


void loop() {
    slave.poll();
}

/**
 설정 값에 따라 Modbus의 알맞은 address에 해당 값을 밀어넣는다.
 @param address 시작 주소
 @param numdata pvalue의 data 갯수
 @param pvalue  데이터 배열
 @return modbus에 값을 밀어넣는 함수 */
uint16_t pushValue(int start, int numdata, uint16_t address,  uint16_t *pvalue){
    
    int x, y, z;            // x : 배열이 시작하는 주소, y: 배열 시작하는 data 위치, z: 배열 끝나는 지점
    if(address < start){    // address값이 시작 주소보다 작을 때, 시작점과 끝나는 점을 고정한다.
        x=start-address;    
        address = 1;            
        y=0;    
        z=numdata+x;    
    }
    else{
        x = 0;
        y = address-start;
        z =start + numdata - address;
    }
    
    for( x ; x < z ; x++){  // 시작하는 address에 따라 배열을 modbus에 출력한다.
        slave.writeRegisterToBuffer(x, pvalue[y++]);
    } 
    
    return STATUS_OK;  
}


/** 
 2~7번까지 각각 회사코드, 제품타입(노드:0, 양액기:1), 제품코드, 프로토콜 버전, 연결장비수, 구역수를 출력
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
 @return 장비 정보
 */
uint8_t processDeviceInfo(uint8_t fc, uint16_t address, uint16_t length) {
    uint16_t devinfo[6] = {1, 1, 10001, 101, 2, 0};     // 회사코드, 제품타입, 제품코드, 프로토콜버전, 연결장비수, 구역수

    return pushValue(2, 6, address, devinfo);          		// 장비정보는 2번부터 시작한다. 
}


/** 
 101~102 번까지 장치코드를 출력.
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
 @return 연결제품코드 
 */
uint8_t processConnectedProduct(uint8_t fc, uint16_t address, uint16_t length) {

    uint16_t devinfo[13] = {101, 202};            //장치코드 : 센서 수에 따라 정해짐 (101,202,303,404,505 ... 1313) 

    return pushValue(101, 13, address, devinfo);         //연결제품코드는 101번부터 시작하며 최대 13개의 장비를 추가할 수 있다.
}


/** 
 201번에 opid, 202번에 노드 상태를 출력.
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
 @retrun 노드상태
 */
uint8_t processNodeStatus(uint8_t fc, uint16_t address, uint16_t length) {
  
    uint16_t devinfo[2] = {0, 0};                   //opid, 노드상태
   
    return pushValue(201, 2, address, devinfo);          //노드상태는 40201번부터 시작한다.
}


/** 
 각 센서의 값과 상태를 출력. 센서 값은 float 32bit이기 때문에 두 값이 걸쳐서 출력되고 그 3번째에 상태값(0/1)이 출력.
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
 @return 센서 정보. 
*/
uint8_t processSensorStatus(uint8_t fc, uint16_t address, uint16_t length) {
 
    float h = dht.readHumidity();                   //습도 측정
    float t = dht.readTemperature();                //온도 측정
  
    /*
    센서를 통해 입력받은 float 데이터 type casting한다.
    센서마다 10의 자리가 바뀌고 1,2번에는 센서값, 3번에는 온도 센서 상태를 나타낸다.
    */
    uint16_t *tem = (uint16_t*) &t;
    uint16_t temperatureValue[3] = {*tem, *(tem+1),0};


    uint16_t *hum = (uint16_t*) &h; 
    uint16_t humidityValue[3] =  {*hum,*(hum+1) , 0};


    pushValue(211, 3, address, temperatureValue);
    pushValue(221, 3, address, humidityValue);
}


/** 
 45001번에 명령코드, 45002번에 opid가 출력.
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
 @return 노드제어 명령 
*/
uint8_t processNodeControl(uint8_t fc, uint16_t address, uint16_t length) {
    
}


/**
 Handel Read Holding Registers (FC=03)
 write back the values from eeprom (holding registers).
 @param fc function code.
 @param address first register/coil address.
 @param length/status length of data / coil status.
*/
uint8_t readMemory(uint8_t fc, uint16_t address, uint16_t length) {
    
    processDeviceInfo(fc, address, length);
    processConnectedProduct(fc, address, length);
    processNodeStatus(fc, address, length);
    processSensorStatus(fc, address, length);
    processNodeControl(fc, address, length);
  
    return STATUS_OK;
}
